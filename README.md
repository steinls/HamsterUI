[TOC]



# HamsterUI介绍

仓鼠ui




## 使用说明
### cartsBall.vue
<img src="https://gitee.com/steinls/HamsterUI/raw/master/ys.gif"  height="667" width="375">



#### 1.引入声明组件

```vue
import darkeCartsBall from '@/components/darke-cartsBall/darke-cartsBall.vue'
export default {
    components: {
   		darkeCartsBall:darkeCartsBall
    }
}
```



#### 2.在页面中使用组件

```vue
//简单使用
<darke-cartsBall ref="cartsBall" 
	:endPos="{
		x: 100, 
		y:200
	}"
>
</darke-cartsBall>


//以下演示可用参数
<darke-cartsBall ref="cartsBall" 
	:ballImage="'url('+require('@/static/logo.png')+')'" 
	:duration="1000" 
	:endPos="{
		x: 100, y:200
	}"
	:is3dSheet="true"
	:duration="1000"
	:zIndex="9999",
	ballColor="red"
>
</darke-cartsBall>
```

ballColor 背景色\n
ballImage 背景图片，会覆盖背景色\n
duration 动画持续时间\n
endPos 终点位置\n
is3dSheet 是否启用3d遮罩\n
zIndex 小球层级\n



#### 3.触发组件函数

```vue
this.$refs.cartsBall.drop({
	x: 20, y:100
})
```



#### 简单示例

1.加入组件目录=>在页面中声明=> 触发组件函数

```vue
<template>
	<view @click="drop($event)" style="height: 700rpx;">
		<darke-cartsBall ref="cartsBall" 
			:endPos="{
				x: 200, y: 300
			}"
		></darke-cartsBall>
	</view>
</template>

<script>
	import darkeCartsBall from '@/components/darke-cartsBall/darke-cartsBall.vue'
	export default {
		components: {
			darkeCartsBall
		},
		methods: {
			drop(){
				this.$refs.cartsBall.drop({
					x: 20, y:100
				})
			}
		}
	}
</script>
```

